CPPFLAGS=-I/usr/include -Wall -DUNIX -g
LDLIBS=-L/usr/lib -lboost_regex -lboost_thread -lboost_system -lpthread 
prefix=/usr/local

SRC  := $(shell find src/ -name "*.cpp")
OBJS := $(subst .cpp,.o,$(SRC))

%.o: %.c
	g++ $(CPPFLAGS) -c $<

bin/UCC: $(OBJS) Makefile
	mkdir -p bin
	g++ $(LDLIBS) $(CPPFLAGS) $(OBJS) -o bin/UCC

all: bin/UCC
	echo "Done"
clean:
	-rm -f bin/UCC bin/UCC.exe
	-rm -rf tmp
	-rmdir bin
	-rm -rf tmp
	-find src -name \*.o -delete -print

install: bin/UCC
	mkdir -p $(prefix)/bin
	install -m 0755 bin/UCC $(prefix)/bin

.PHONY: install all clean

